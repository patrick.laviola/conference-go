from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY} # Create a dictionary for the headers to use in the request
    url = f'https://api.pexels.com/v1/search?query={city}+{state}&per_page=1' # Create the URL for the request with the city and state
    response = requests.get(url, headers=headers) # Make the request
    if response.status_code == 200: 
        data = json.loads(response.content) # Parse the JSON response
        picture_url = data['photos'][0]['src']['original'] 
        return {
            'picture_url': picture_url
        }# Return a dictionary that contains a `picture_url` key and one of the URLs for one of the pictures in the response
    else:
        print("Error fetching photo")
        return None


def kelvin_to_fahrenheit(kelvin):
    return (kelvin - 273.15) * 9/5 + 32


def get_weather_data(city, state):
    url_geo = f'http://api.openweathermap.org/geo/1.0/direct?q={city},{state}&limit=1&appid={OPEN_WEATHER_API_KEY}' # Create the URL for the geocoding API with the city and state
    response_geo = requests.get(url_geo)  # Make the request
    data_geo = json.loads(response_geo.content)  # Parse the JSON response
    if data_geo:
        latitude = data_geo[0]['lat']  # Get the latitude from the response
        longitude = data_geo[0]['lon']  # Get the longitude from the response
    else:
        return "No lat or lon coordinates found"

    url_weather = f'http://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}' # Create the URL for the current weather API with the latitude and longitude
    response_weather = requests.get(url_weather)  # Make the request
    data_weather = json.loads(response_weather.content)  # Parse the JSON response
    kelvin_temperature = data_weather['main']['temp'] # Get the main temperature and the weather's description and put them in a dictionary
    fahrenheit_temperature = kelvin_to_fahrenheit(kelvin_temperature)
    weather_description = data_weather['weather'][0]['description']
    return {'temperature': fahrenheit_temperature, 'weather_description': weather_description}  # Return the dictionary
