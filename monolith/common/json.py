from json import JSONEncoder
from datetime import datetime
from django.db.models import QuerySet


class DateEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()
        else:
            return super().default(o)
"""
DateEncoder: This encoder is used to handle datetime objects. When an instance of a datetime object is encountered, it is converted to its ISO 8601 string representation using the isoformat method. This allows datetime objects to be serialized to a JSON-compatible string format, as JSON doesn't have a native datetime type.
"""


class QuerySetEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, QuerySet):
            return list(o)
        else:
            return super().default(o)
"""
QuerySetEncoder: Handles Django QuerySet objects by converting them into a list. This can be useful when you want to serialize a query set directly to JSON. It uses the list function to convert the query set to a list of model instances, which can then be handled by other encoders like ModelEncoder.
"""


class ModelEncoder(DateEncoder, QuerySetEncoder, JSONEncoder):
    encoders = {} # Initialize a class-level dictionary to store property-specific encoders.

    def default(self, o): # Define the default method, which is called by JSONEncoder for objects not serializable by default.
        if isinstance(o, self.model):   # Check if the object to encode is of the type specified in the model attribute. If the object to decode is the same class as what's in the model property, then
            d = {} # create an empty dictionary that will hold the property names as keys and the property values as values
            if hasattr(o, "get_api_url"): # Check if the object has a "get_api_url" method.
                d["href"] = o.get_api_url() # If so, call that method and store the resulting URL under the "href" key.
            for property in self.properties:    # for each name in the properties list self.properties
                value = getattr(o, property)  # get the value of that property from the model instance (the object) using just the property name
                if property in self.encoders: # Check if there's a specific encoder for this property in the encoders dictionary.
                    encoder = self.encoders[property] # If so, retrieve the corresponding encoder.
                    value = encoder.default(value) # Use that encoder to serialize the value.
                d[property] = value   # put it into the dictionary with that property name as the key
            d.update(self.get_extra_data(o)) # Calls the get_extra_data method with the object being serialized, and updates the dictionary 'd' with the returned data.
            # This allows for additional data to be included in the serialized output, depending on the implementation of the get_extra_data method.
            return d  # Return the dictionary, which now represents the serialized form of the object.
        else:
            return super().default(o)  # If the object is not of the type specified in model, call the superclass's default method, delegating to other encoders or default JSON encoding logic.
    
    def get_extra_data(self, o):
        return {} # takes an object and returns an empty dictionary by default
"""
ModelEncoder: 
This encoder extends DateEncoder, and it is designed to convert instances of specified Django models into dictionaries. 
- The properties to be included in the dictionary are listed in the properties attribute
- The model type that should be handled by this encoder is defined in the model attribute. 
- The default method of this encoder checks if the object is an instance of the specified model and then constructs a dictionary using the specified properties, delegating any datetime objects to DateEncoder to handle. 
- If the object is not an instance of the specified model, the encoder calls the superclass's default method, which can handle other types or pass them up the chain to other encoders or the default JSON encoding logic.
"""